<?php

namespace Database\Factories\League;

use App\Models\League\Player;
use App\Models\League\Team;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlayerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Player::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'   => $this->faker->name,
            'number' => $this->faker->numberBetween(1, 99),
        ];
    }
}
