<?php

namespace Database\Seeders;

use App\Models\League\Player;
use App\Models\League\Team;
use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::factory()
            ->has(Player::factory()->count(23))
            ->count(30)
            ->create();
    }
}
