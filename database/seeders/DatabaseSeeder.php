<?php

namespace Database\Seeders;

use Database\Factories\League\LeagueFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         \App\Models\User\User::factory(10)->create();
        $this->call([
            LeagueSeeder::class,
            TeamSeeder::class,
            PlayerSeeder::class,
        ]);
    }
}
