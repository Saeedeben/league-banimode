<?php

namespace App\Models\League;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class League
 *
 * @package App\Models\League
 *
 * @property int    $id
 *
 * @property string $name
 * @property int    $number
 *
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * ------------------------------------ Relations ------------------------------------
 * @property Team   $team
 */
class Player extends Model
{
    use HasFactory;

    protected $table = 'players';


    protected $fillable = [
        'name',
        'number',
    ];

    // ------------------------------------ Relations ------------------------------------
    public function team()
    {
        return $this->belongsTo(Team::class);
    }
    // ------------------------------------ Attributes ------------------------------------
    // ------------------------------------ Methods ------------------------------------

}
