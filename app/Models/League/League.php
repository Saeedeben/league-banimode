<?php

namespace App\Models\League;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class League
 *
 * @package App\Models\League
 *
 * @property int    $id
 *
 * @property string $name
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * ------------------------------------  ------------------------------------
 * @property Team   $teams
 */
class League extends Model
{
    use HasFactory;

    protected $table = 'leagues';

    protected $fillable = [];


    // ------------------------------------ Relations ------------------------------------
    public function teams()
    {
        return $this->hasManyThrough(Player::class, Team::class);
    }
    // ------------------------------------ Attributes ------------------------------------
    // ------------------------------------ Methods ------------------------------------

}
