<?php

namespace App\Http\Controllers\League;

use App\Http\Controllers\Controller;
use App\Http\Requests\League\LeagueListRequest;
use App\Http\Resources\League\LeagueListResource;
use App\Models\League\League;
use Illuminate\Http\Request;

class LeagueController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list(LeagueListRequest $request)
    {
        /** @var League $league */
        $league = League::all();

        /** @var LeagueListResource $leagueResource */
        $leagueResource = LeagueListResource::collection($league);

        return $leagueResource;
    }
}
