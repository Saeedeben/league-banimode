<?php

namespace App\Http\Controllers\League;

use App\Http\Controllers\Controller;
use App\Http\Requests\Player\PlayerStoreRequest;
use App\Http\Requests\Player\PlayerUpdateRequest;
use App\Models\League\Player;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function store(PlayerStoreRequest $request)
    {
        $attributes = $request->all();

        try {
            \DB::beginTransaction();

            $player = new Player();
            $player->fill($attributes);
            $player->team()->associate($attributes['team_id']);
            $player->save();

            \DB::commit();

            return [
                'success' => true,
                'message' => 'بازیکن با موفقیت ذخیره شد',
            ];
        } catch (\Exception $ex) {
            \DB::rollback();

            \Log::info("$ex");

            return [
                'success' => true,
                'message' => 'خطایی رخ داد',
            ];

        }
    }

    public function update(PlayerUpdateRequest $request, Player $player)
    {
        $attributes = $request->all();

        try {
            \DB::beginTransaction();

            $player->fill($attributes);
            if ($attributes['team_id'])
                $player->team()->associate($attributes['team_id']);
            $player->save();

            \DB::commit();

            return [
                'success' => true,
                'message' => 'بازیکن با موفقیت به روزرسانی شد',
            ];
        } catch (\Exception $ex) {
            \DB::rollback();

            \Log::info("$ex");

            return [
                'success' => true,
                'message' => 'خطایی رخ داد',
            ];

        }
    }

    public function destroy(Player $player)
    {
        $player->delete();

        return [
            'success' => true,
            'message' => 'بازیکن با موفقیت حذف شد',
        ];
    }
}
