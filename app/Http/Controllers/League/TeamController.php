<?php

namespace App\Http\Controllers\League;

use App\Http\Controllers\Controller;
use App\Http\Requests\Team\TeamListRequest;
use App\Http\Requests\Team\TeamPlayerListRequest;
use App\Http\Requests\Team\TeamStoreRequest;
use App\Http\Requests\Team\TeamUpdateRequest;
use App\Http\Resources\Team\TeamListResource;
use App\Http\Resources\Team\TeamPlayerListResource;
use App\Models\League\Player;
use App\Models\League\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list(TeamListRequest $request)
    {
        /** @var Team $teams */
        $teams = Team::all();

        /** @var TeamListResource $teamResource */
        $teamResource = TeamListResource::collection($teams);

        return $teamResource;
    }

    public function store(TeamStoreRequest $request)
    {
        $attributes = $request->all();

        try {
            \DB::beginTransaction();

            $team = new Team();
            $team->fill($attributes);
            $team->league()->associate($attributes['league_id']);
            $team->save();

            \DB::commit();

            return [
                'success' => true,
                'message' => 'تیم با موفقیت ثبت شد',
            ];

        } catch (\Exception $ex) {
            \DB::rollback();

            \Log::info("$ex");

            return [
                'success' => false,
                'message' => 'خطایی رخ داد',
            ];
        }
    }

    public function update(TeamUpdateRequest $request, Team $team)
    {
        $attributes = $request->all();

        try {
            \DB::beginTransaction();

            $team->fill($attributes);
            if ($attributes['league_id'])
                $team->league()->associate($attributes['league_id']);
            $team->save();

            \DB::commit();

            return [
                'success' => true,
                'message' => 'تیم با موفقیت به روزرسانی شد',
            ];

        } catch (\Exception $ex) {
            \DB::rollback();

            \Log::info("$ex");

            return [
                'success' => true,
                'message' => 'خطایی رخ داد',
            ];
        }
    }

    public function teamPlayersList(TeamPlayerListRequest $request, Team $team)
    {
        $players = $team->players;

        /** @var TeamPlayerListResource $teamPlayerResource */
        $teamPlayerResource = TeamPlayerListResource::collection($players);

        return $teamPlayerResource;
    }
}
