<?php

namespace App\Http\Resources\Team;

use App\Models\League\Team;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TeamListResource
 *
 * @package App\Http\Resources\Team
 * @mixin Team
 */
class TeamListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $this->id,
            'label' => $this->name,

        ];
    }
}
