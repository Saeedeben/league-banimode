<?php

namespace App\Http\Resources\Team;

use App\Models\League\Player;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TeamPlayerListResource
 *
 * @package App\Http\Resources\Team
 *
 * @mixin Player
 */
class TeamPlayerListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'     => $this->id,
            'name'   => $this->name,
            'number' => $this->number,
        ];
    }
}
