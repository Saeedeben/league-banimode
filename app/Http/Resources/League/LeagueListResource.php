<?php

namespace App\Http\Resources\League;

use App\Models\League\League;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class LeagueListResource
 *
 * @package App\Http\Resources\League
 *
 * @mixin League
 */
class LeagueListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $this->id,
            'label' => $this->name,
        ];
    }
}
