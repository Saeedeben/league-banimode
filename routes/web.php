<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::get('league/list', [\App\Http\Controllers\League\LeagueController::class, 'list']);

    Route::group(['prefix' => 'team'], function () {
        Route::get('list', [\App\Http\Controllers\League\TeamController::class, 'list']);
        Route::post('store', [\App\Http\Controllers\League\TeamController::class, 'store']);
        Route::put('/update/{team}', [\App\Http\Controllers\League\TeamController::class, 'update']);
        Route::get('/players_list/{team}', [\App\Http\Controllers\League\TeamController::class, 'teamPlayersList']);
    });

    Route::group(['prefix' => 'player'], function () {
        Route::post('store', [\App\Http\Controllers\League\PlayerController::class, 'store']);
        Route::put('/update/{player}', [\App\Http\Controllers\League\PlayerController::class, 'update']);
        Route::delete('/delete/{player}', [\App\Http\Controllers\League\PlayerController::class, 'destroy']);
    });
});
